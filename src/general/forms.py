from django import forms

from .models import LetterToAdmin


class LetterToAdminAnonForm(forms.ModelForm):
    class Meta:
        model = LetterToAdmin
        fields = ('sender', 'email', 'faculty', 'message')
        labels = {'sender': 'Nimi', 'email': 'E-mail', 'faculty': 'Valdkond', 'message': 'Sõnum'}


class LetterToAdminUserForm(forms.ModelForm):
    class Meta:
        model = LetterToAdmin
        fields = ('faculty', 'message')
        labels = {'faculty': 'Valdkond', 'message': 'Sõnum'}

