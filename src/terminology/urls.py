from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(?P<id>[\w+]+)/$', views.term_detail, name='term_detail'),
]