from django.contrib.auth.models import User
from django.db import models


class MessageType:
    TECHNICAL = 't'
    CONTENT = 'c'
    REGISTRATION = 'r'
    CHOICES = (
        (TECHNICAL, 'tehniline'),
        (CONTENT, 'sisuline'),
        (REGISTRATION, 'kasutajaks registreerumine'),
    )


class LetterToAdmin(models.Model):
    user = models.ForeignKey(User, blank=True, null=True)
    sender = models.CharField(max_length=100, blank=True)
    email = models.EmailField(blank=True)
    faculty = models.CharField(max_length=1, choices=MessageType.CHOICES)
    message = models.TextField()
    time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.sender
