from django.contrib import admin
from .models import Sangha, School

admin.site.register(Sangha)
admin.site.register(School)