from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^(?P<lang>[\w+]+)/$', views.term_list, name='term_list'),
]