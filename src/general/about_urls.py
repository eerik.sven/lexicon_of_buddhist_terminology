from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^kaasautorile/$', views.for_contributor, name='contributor'),
    url(r'^lugejale/$', views.for_reader, name='for_reader'),
    url(r'^kiri_haldajale/$', views.letter_to_admin, name='letter_to_admin'),

]