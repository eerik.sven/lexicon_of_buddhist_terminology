# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-30 21:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0003_auto_20171130_2317'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lettertoadmin',
            name='user',
        ),
    ]
