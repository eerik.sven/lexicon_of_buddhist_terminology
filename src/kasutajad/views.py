from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.utils.text import slugify

from .models import UserAdds
from .forms import NewUserForm, LogInForm, LogOutForm


YOU_ARE_INACTIVE = 'Sinu kasutajakonto ei ole aktiivne.'
WRONG_DATA = 'Vale kasutajatunnus või salasõna.'


def log_in(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                else:
                    return render(request, 'kasutajad/log_in_failed.html', {'message': YOU_ARE_INACTIVE})  # not working
                return redirect('homepage')
            else:
                return render(request, 'kasutajad/log_in_failed.html', {'message': WRONG_DATA})
    else:
        form = LogInForm()
    return render(request, 'kasutajad/log_in.html', {'form': form})


@login_required(login_url='/kasutajad/sisselogimine/')
def user_page(request, user_slug):
    user = User.objects.filter(username=user_slug)[0]
    if user.username != request.user.username:
        return redirect('no_permission')
    else:
        if request.method == "POST":
            form = LogOutForm(request.POST)
            if form.is_valid():
                logout(request)
                return redirect('homepage')
        else:
            form = LogOutForm()
        return render(request, 'kasutajad/user_page.html', {'user': user, 'form': form})
