﻿
# Kaasautorile

## Terminite kirjete muutmine

### Ristviited
Termini selgitustes on võimalik kasutada viiteid lehekülje teistele kirjetele, kasutades järgnevat süntaksit:
`[[id#kuvatav tekst]]` või `[[id|kuvatav tekst]]`. *Id* on termini rakendusesisene kood, mille abil koostatakse URL.
 Iga termini *id* on tema URLi viimane lõik. *Kuvatav tekst* on termin teksti sobivas grammatilises vormis.

Näide:

* **Kasutaja kirjutab:** `See on [[9#Virgunute]] õpetus.`
* **HTML:** `See on <a href="/terminid/9/">Virgunute</a> õpetus.`
* **Veebilehitseja kuvab:** See on <a href="/terminid/9/">Virgunute</a> õpetus.

Termini selgitustes on võimalik kasutada HTML süntaksit.

## Kasutajad ja nende õigused
Iga järgnev kasutajarühm omab ka kõikide eelnenud rühmade õigusi.

1. **Registreerumata kasutaja:** saab kommenteerida ja lehe administraatroitega ühendust võtta;
2. **Tavakasutaja:** saab kommenteerida ja lehe administraatroitega ühendust võtta, ilma et peaks iga kord oma nime ja e-maili uuesti sisestama;
3. **Kaasautor:** saab muuta leksikoni kandeid;
4. **Administraator (haldaja):** saab kasutajale anda ja ära võtta kaasautori õigusi, saab kustutada kommentaare;
5. **Arendaja:** omab piiramatut ligipääsu kõikidele projketiga seonduvatele failidele ning serverile.
