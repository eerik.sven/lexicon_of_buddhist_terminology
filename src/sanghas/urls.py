from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.list_of_sanghas, name='list_of_sanghas'),
]