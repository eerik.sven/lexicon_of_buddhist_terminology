from django.db import models


class School(models.Model):
    name = models.CharField(max_length=150, unique=True)

    # belongs_to = models.ForeignKey(School, blank=True)
    # How to add Foreign Key to the same class?

    def __str__(self):
        return self.name


class Sangha(models.Model):
    name = models.CharField(max_length=200, unique=True)
    school = models.ForeignKey(School, blank=True)
    location = models.CharField(max_length=200)  # town
    website = models.URLField(blank=True)
    e_mail = models.EmailField(blank=True)

    def __str__(self):
        return self.name
