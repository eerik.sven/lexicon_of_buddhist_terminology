from django.core.exceptions import ValidationError
from django.db import models
from django.utils.text import slugify


class Term(models.Model):
    explanation = models.TextField(blank=True)
    translation_notes = models.TextField(blank=True)

    def __str__(self):
        title = self.inestonian_set.filter(is_best=True)
        if not title:
            title = self.inestonian_set.all()
        return title[0].translation.capitalize()


class TranslatedTerm(models.Model):
    translation = models.CharField(max_length=200, unique=True)
    slug = models.SlugField()
    term = models.ForeignKey(Term, blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.translation

    def save(self, *args, **kwargs):
        self.slug = slugify(self.translation)
        super().save(*args, **kwargs)



class InEstonian(TranslatedTerm):
    is_best = models.BooleanField()


class InPali(TranslatedTerm):
    pass


class InSanskrit(TranslatedTerm):
    pass


class InChinese(TranslatedTerm):
    pass


class InJapanese(TranslatedTerm):
    pass


class InTibetan(TranslatedTerm):
    pass


class InEnglish(TranslatedTerm):
    pass


class AlternativeExplanations(models.Model):
    term = models.ForeignKey(Term)
    author = models.CharField(max_length=200, blank=True)
    explanation = models.TextField()


class Comment(models.Model):
    term = models.ForeignKey(Term)
    body = models.TextField()
    date = models.DateTimeField()
    user = models.ForeignKey('auth.User', blank=True, null=True)
    sender = models.CharField(max_length=100)
    email = models.EmailField()
