# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-12-25 15:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('terminology', '0017_auto_20171223_2106'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='inchinese',
            name='in_chinese',
        ),
        migrations.RemoveField(
            model_name='inchinese',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='inchinese',
            name='term',
        ),
        migrations.RemoveField(
            model_name='inenglish',
            name='in_english',
        ),
        migrations.RemoveField(
            model_name='inenglish',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='inenglish',
            name='term',
        ),
        migrations.RemoveField(
            model_name='inestonian',
            name='in_estonian',
        ),
        migrations.RemoveField(
            model_name='inestonian',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='inestonian',
            name='term',
        ),
        migrations.RemoveField(
            model_name='injapanese',
            name='in_japanese',
        ),
        migrations.RemoveField(
            model_name='injapanese',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='injapanese',
            name='term',
        ),
        migrations.RemoveField(
            model_name='inpali',
            name='in_pali',
        ),
        migrations.RemoveField(
            model_name='inpali',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='inpali',
            name='term',
        ),
        migrations.RemoveField(
            model_name='insanskrit',
            name='in_sanskrit',
        ),
        migrations.RemoveField(
            model_name='insanskrit',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='insanskrit',
            name='term',
        ),
        migrations.RemoveField(
            model_name='intibetan',
            name='in_tibetan',
        ),
        migrations.RemoveField(
            model_name='intibetan',
            name='slug',
        ),
        migrations.RemoveField(
            model_name='intibetan',
            name='term',
        ),
    ]
