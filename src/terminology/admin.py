from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError

from .models import Term, AlternativeExplanations, Comment, InPali, InSanskrit, InChinese, InJapanese, InTibetan, \
    InEnglish, InEstonian


class TranslationAdmin(admin.TabularInline):
    extra = 1
    readonly_fields = ('slug',)


class RequireOneFormSet(forms.models.BaseInlineFormSet):
    def clean(self):
        super().clean()
        if not self.is_valid():
            return
        if not self.forms or not self.forms[0].cleaned_data:
            raise ValidationError('At least one {} required'.format(self.model._meta.verbose_name))


class InEstonianAdmin(TranslationAdmin):
    model = InEstonian
    formset = RequireOneFormSet


class InPaliAdmin(TranslationAdmin):
    model = InPali


class InSanskritAdmin(TranslationAdmin):
    model = InSanskrit


class InChineseAdmin(TranslationAdmin):
    model = InChinese


class InJapaneseAdmin(TranslationAdmin):
    model = InJapanese


class InTibetanAdmin(TranslationAdmin):
    model = InTibetan


class InEnglishAdmin(TranslationAdmin):
    model = InEnglish


class AlternativeExplanationsInline(admin.TabularInline):
    model = AlternativeExplanations


class CommentAdmin(admin.TabularInline):
    model = Comment
    extra = 1


class TermAdmin(admin.ModelAdmin):
    save_on_top = True
    search_fields = ('id',)
    inlines = (
        InEstonianAdmin, InPaliAdmin, InSanskritAdmin, InChineseAdmin, InJapaneseAdmin, InTibetanAdmin, InEnglishAdmin,
        AlternativeExplanationsInline, CommentAdmin)


admin.site.register(Term, TermAdmin)

"""
admin.site.register(InEstonian)
admin.site.register(InPali)
admin.site.register(InSanskrit)
admin.site.register(InChinese)
admin.site.register(InJapanese)
admin.site.register(InTibetan)
admin.site.register(InEnglish)
"""
